import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */

    public void run() {
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(2, 1);
        System.out.println(g);
        g.calculateGraphRadius(g.getDistanceMatrix(g.createAdjMatrix()));
    }

    /**
     * Vertex represents the node of a graph
     */

    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

    }


    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */

        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Converts adjacency matrix to the matrix of distances.
         *
         * @param adjacentMatrix adjacency matrix
         * @return matrix of distances, where each edge has length 1
         */

        public int[][] getDistanceMatrix(int[][] adjacentMatrix) {

            int numberOfVertices = adjacentMatrix.length;
            int[][] distanceMatrix = new int[numberOfVertices][numberOfVertices];

            if (numberOfVertices < 1 || adjacentMatrix[0].length != numberOfVertices) {
                throw new IllegalArgumentException("Error - parameter adjacentMatrix is not correct:" + adjacentMatrix);
            }
            int INFINITY = 2 * numberOfVertices + 1;
            for (int i = 0; i < numberOfVertices; i++) {
                for (int j = 0; j < numberOfVertices; j++) {

                    if (i == j) {
                        distanceMatrix[i][j] = 0;
                    } else if (adjacentMatrix[i][j] == 0) {
                        distanceMatrix[i][j] = INFINITY;
                    } else {
                        distanceMatrix[i][j] = 1;
                    }

                }
            }
            return distanceMatrix;
        }

        /**
         * Calculates shortest paths using Floyd-Warshall algorithm.
         * This matrix will contain shortest paths between each pair of vertices.
         */

        public void shortestPaths(int[][] distanceMatrix) {
            int n = distanceMatrix.length;

            if (n < 1 || distanceMatrix[0].length != n) {
                throw new IllegalArgumentException("Error - parameter distanceMatrix is not correct:" + distanceMatrix);
            }

            printNestedArray(distanceMatrix);
            for (int k = 0; k < n; k++) {
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < n; j++) {
                        int newlength = distanceMatrix[i][k] + distanceMatrix[k][j];
                        if (distanceMatrix[i][j] > newlength) {
                            distanceMatrix[i][j] = newlength;
                        }
                    }
                }
            }
        }

        /**
         * Calculates the radius of a graph
         *
         * @param distanceMatrix distance matrix
         */

        public void calculateGraphRadius(int[][] distanceMatrix) {
            shortestPaths(distanceMatrix);

            System.out.println("The following matrix shows the shortest " +
                    "distances between every pair of vertices");
            printNestedArray(distanceMatrix);

            int[] array = getMaximumDistances(distanceMatrix);
            System.out.println("The following array shows the eccentricites of graph vertices:");
            printArray(array);

            int radius = getMinimumElement(array);
            System.out.println("The following number is the radius of the graph: " + radius);
        }

        void printArray(int array[]) {
            for (int i = 0; i < array.length; i++) {
                System.out.print(array[i] + " ");
            }
            System.out.println();
        }

        void printNestedArray(int distanceMatrix[][]) {
            int vertices = distanceMatrix.length;
            for (int i = 0; i < vertices; ++i) {
                for (int j = 0; j < vertices; ++j) {
                    System.out.print(distanceMatrix[i][j] + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }

        /**
         * Filters out the maximum distances of from given array
         *
         * @param array distance matrix expressing shortest paths between two vertices
         * @return array of vertices eccentricities
         */

        public int[] getMaximumDistances(int[][] array) {
            int max = 0;

            if (array.length < 1 || array[0].length != array.length) {
                throw new IllegalArgumentException("Error - parameter array is not correct:" + array);
            }

            int[] maximumDistances = new int[array.length];

            for (int i = 0; i < array.length; i++) {
                max = array[i][0];
                for (int j = 0; j < array[i].length; j++) {
                    if (array[i][j] > max) {
                        max = array[i][j];
                    }
                }
                maximumDistances[i] = max;
            }

            return maximumDistances;
        }

        /**
         * Returns the minimum element from array
         *
         * @param array
         * @return minimum element
         */

        public int getMinimumElement(int[] array) {

            if (array.length < 1) {
                throw new IllegalArgumentException("Error - parameter array is not correct:" + array);
            }

            int min = array[0];

            for (int i = 0; i < array.length; i++) {
                if (array[i] < min) {
                    min = array[i];
                }
            }
            return min;
        }

    }

} 

